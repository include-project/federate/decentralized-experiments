# Decentralized Logistic Regression 

This repository contains example scripts for running the decentralized learning package [provided here](https://gitlab.com/include-project/federate/decentralized-learning).

Before running these scripts, make sure the `decentralized` package is installed. It can be installed in a R shell by using the `install_gitlab` command from the package `devtools`:

	devtools::install_gitlab("include-project/federate/decentralized-learning")
	
The script `reglog_heart/reglog_heart.R` is an example of the results the `decentralized` package can give. It downloads the data from the [UCI Heart Disease dataset](https://archive.ics.uci.edu/ml/datasets/heart+Disease), then simulates the decentralized computation of a logistic regression on the heart disease dataset.

The script `reglog_heart/reglog_heart_network.R` does the same computation, but running each agent taking part in the computation in a different thread, running servers that query each others to run the computation. This can be used directly to perform decentralized optimization task on different centers.

As the `decentralized` package only aims at proving that such computation is possible, it does not implement any data and communication security mechanisms, and must thus be used very wisely.
